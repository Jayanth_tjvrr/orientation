![Docker Image](dockerimage.png)

<br>

# Docker Introduction

Docker helps developers bring their ideas to life by conquering the complexity of app development. We simplify and accelerate development workflows with an integrated dev pipeline and through the consolidation of application components. Actively used by millions of developers around the world, Docker Desktop and Docker Hub provide unmatched simplicity, agility and choice.

<br>
Containerization is increasingly popular because containers are:

- **Flexible**: Even the most complex applications can be containerized.
- **Lightweight**: Containers leverage and share the host kernel,
  making them much more efficient in terms of system resources than virtual machines.
- **Portable**: You can build locally, deploy to the cloud, and run anywhere.
- **Loosely coupled**: Containers are highly self sufficient and encapsulated,
  allowing you to replace or upgrade one without disrupting others.
- **Scalable**: You can increase and automatically distribute container replicas across a datacenter.
- **Secure**: Containers apply aggressive constraints and isolations to processes without any configuration required on the part of the user.

<br>

## Docker Images :
An image includes everything needed to run an application - the code or binary,
runtimes, dependencies, and any other filesystem objects required.They are available in the docker registry or can be created by a developer for their requirements.

## Docker Containers :
A container is a standard unit of software that packages up code and all its dependencies so the application runs quickly and reliably from one computing environment to another. A Docker container image is a lightweight, standalone, executable package of software that includes everything needed to run an application: code, runtime, system tools, system libraries and settings.
<br>
![docker container](container.png)
<br>
Container images become containers at runtime and in the case of Docker containers - images become containers when they run on Docker Engine. Available for both Linux and Windows-based applications, containerized software will always run the same, regardless of the infrastructure. Containers isolate software from its environment and ensure that it works uniformly despite differences for instance between development and staging.
<br><br>
Docker containers that run on Docker Engine:

- **Standard**: Docker created the industry standard for containers, so they could be portable anywhere
- **Lightweight**: Containers share the machine’s OS system kernel and therefore do not require an OS per application, driving higher server efficiencies and reducing server and licensing costs
- **Secure**: Applications are safer in containers and Docker provides the strongest default isolation capabilities in the industry

<br>
<br>

## Docker Commands for accessing Docker Tool :
We can access the docker Runtime environment via Docker Desktop Applications or via CLI for running applications for different purposes like development, production, testing, monitoring..etc.,

```shell
$ docker --version
Docker version 19.03.13, build 4484c46d9d
```
<br>
For Listing all the images

```shell
$ docker image ls
REPOSITORY                           TAG                 IMAGE ID            CREATED             SIZE
hello-world                          latest              bf756fb1ae65        10 months ago       13.3kB
```

<br>
For Listing all the Containers either running or stopped

```shell
$ docker ps -a
CONTAINER ID      IMAGE            COMMAND     CREATED       STATUS                  PORTS     NAMES
6f1d8b598f26      hello-world      "/hello"    3 months ago  Exited (0) 3 months ago           modest_mendel
```

<br>
<br>

## About Dockerfile :

Docker builds images automatically by reading the instructions from a Dockerfile 
    -- a text file that contains all commands, in order, needed to build a given image. 
<br>
A Dockerfile adheres to a specific format and set of instructions which you can find at Dockerfile reference.

Example of Dockerfile creating a web application Image:

```shell
FROM ubuntu:18.04
COPY . /app
RUN make /app
CMD python /app/app.py
```

<br>

Building the Image from Dockerfile:

```shell
$ docker build -t helloapp:v1 .
```

<br>
Running the container from the Image:

```shell
docker run --name helloappcontainer helloapp:v1 
```