# Git Introduction

Git is a version-control system for tracking changes in computer files and coordinating work on those files among multiple people. Git is a Distributed Version Control System.

<br>

## What is a repository?
A repository a.k.a. repo is nothing but a collection of source code.

<br>

## What is a Git branch?

A Git branch is essentially an independent line of development. You can take advantage of branching when working on new features or bug fixes because it isolates your work from that of other team members.

<br>

![Gitbranch](branching.jpeg)

<br>

Different branches can be merged into any one branch as long as they belong to the same repository.

<br>

## There are four fundamental elements in the Git Workflow.

<br>

- **Working Directory:** 
When a developer clones the codebase from the remote repository, it will be copied to the working directory of the local system. While cloning the codebase from the remote repository, .git folder will also be cloned from the remote repository. 
- **Staging Area:**
The Staging area is a phase in GIT workflow where changes of the code will be stored in the index. The Index will carry only the changes that happened in the codebase since the last time. When you pass the add command, GIT will collect all the changes in the files and keeps it in the HEAD.
- **Local Repository:**
Local Repository is the phase or the place where all the changes and the information regarding the changes are stored in the local repository. This local repository is been created from the remote repository which is the main copy of the codebase. This local repository is created from the remote repository by cloning though GIT client tool.


- **Remote Repository:**
Remote Repository is stored in GIT remote server or centralized server, where the main copy of the code is been stored. After the commit to the local repository, Developer will send the changed code to the remote server by passing push command of GIT Client tool.

<br>

<br>

## GIT clone, GIT pull and GIT Fetch
GIT clone is the process of creating a working copy of the remote or local repository by passing the following command.
```shell
git clone /path_of_repository
git clone username@git_server_hostname:/path_of_repository
```

If we have already cloned the repository and need to update local (only code) respect to the remote server, we need to get pull from the remote server by passing following command
<br>

```shell
git pull origin master
```
When in the above command, git pull is the command, the origin is the remote reference/URL of remote server and master is the branch name.
<br>
GIT fetch is the process of updating (only git information) the local GIT structure and information from remote repository. By passing the following command, we can fetch the remote repository.
<br><br>

## GIT Checkout
Git checkout is the event of getting or changing the current state of the git branch to another. When we want to create a branch and move to the created branch, we use the following command.

```shell
git checkout -b <new_branch> 
```

this will create a branch called <new_branch> and current HEAD will move to the newly created branch. Which means, the changes after this command will be captured in the newly created branch.
<br><br>

## GIT Add and GIT Commit
When we want to add the changes of code to the index of the GIT, we will pass the following command.
```shell
git add <file1> <file2> <file3>
or

git add * 
```
Basically, this is the first step of the GIT workflow.
<br>
GIT commit is the event of adding the index to the HEAD of the local repository. This will be done by passing following command
```shell
git commit -m “your commit message”
```
But, this commit will be done only on the local repository. Which mean, we need to push the commits and updated HEAD to the remote repository.
<br><br>

![Gitworkflow](GitWF.png)

## GIT push.
When we commit changes to the local repository, it will have the information about changes in the codebase and its change message with it. So, if we want to update or send the changes to the remote repository, we need to pass the following command.
```shell
git push origin <new_branch>
```
Like we have seen in GIT Pull, the origin is the alias name of the location of the remote server. When we point the <new_branch>, GIT push command will create a new branch in the remote server and store the changes.
<br>
This is the basic workflow, but this is not the end of the workflow. We need to merge the <new_branch> to the master branch.
<br>

## GIT Merge
GIT Merge will merge the update in <new_branch> to the master branch or whatever the branch we need to merge with. To merge the branch with the current branch. Checkout to the working branch and pass the following command
```shell
git merge <master_or_destination_branch>
```
This will merge and add the changes that are present in the <new_branch> to <master_or_destination_branch>. Ideally, this process will be taken care of by hosting service tools like GitHub or BitBucket.






